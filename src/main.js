import { createApp } from "vue";
import App from "./App.vue";

createApp(App).mount("#app");

const scrollContainer = document.querySelector("#app");

scrollContainer.addEventListener("wheel", (evt) => {
	if (window.innerWidth > 1500) {
		evt.preventDefault();
		scrollContainer.scrollLeft +=
			(window.innerWidth * evt.deltaY) / Math.abs(evt.deltaY);
	}
});
